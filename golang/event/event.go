package event

import (
	"crypto/rand"

	"github.com/google/uuid"
)

type EventType string

const (
	Stop    EventType = "stop"
	Publish EventType = "publish"
)

func (e EventType) String() string {
	return string(e)
}

type NotifyEvent struct {
	Stream uuid.UUID `json:"stream_id"`
	Type   EventType `json:"type"`
}

func RandomizedPublishEvent() (NotifyEvent, error) {
	streamId, err := uuid.NewRandomFromReader(rand.Reader)

	if err != nil {
		return NotifyEvent{}, err
	}

	return NotifyEvent{
		Stream: streamId,
		Type: Publish,
	}, nil
}

func RandomizedStopEvent() (NotifyEvent, error) {
	streamId, err := uuid.NewRandomFromReader(rand.Reader)

	if err != nil {
		return NotifyEvent{}, err
	}

	return NotifyEvent{
		Stream: streamId,
		Type: Stop,
	}, nil
}
