package main

import (
	"encoding/json"
	"os"
	"strings"
	"math/rand"
	"time"
	"fmt"
	"github.com/nats-io/nats.go"
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	"gitlab.com/zerodevs/task-steal/event"
)

var (
	client * nats.Conn
	stream nats.JetStreamContext

	ackWaitDuration time.Duration
	idleDuration time.Duration
	keepAliveDuration time.Duration

	workDurationMinuteMax int

	natsUrl string = "nats://127.0.0.1:4222"
)

func workDuration() time.Duration {
	return time.Duration(rand.Intn(workDurationMinuteMax)) * time.Minute
}

func ParseDefaultsFromEnvs() {
	natsUrl := os.Getenv("NATS_URL")
	natsUrl = strings.TrimPrefix(natsUrl, " ")

	if natsUrl == "" {
		natsUrl = "nats://127.0.0.1:4222"
	}

	log.Debug().
		Str("url", natsUrl).
		Msg("debug envs")

}

func init() {
	var err error

	log.Logger = log.Output(zerolog.ConsoleWriter{Out: os.Stderr})

	zerolog.SetGlobalLevel(zerolog.TraceLevel)


	// 10 minutes work duration
	workDurationMinuteMax = 30

	idleDuration = 30 * time.Second

	// 2 minutes keep alive work duration
	keepAliveDuration = 15 * time.Second

	// 6 minutes ack wait duration
	ackWaitDuration = 1 * time.Minute

	client, err = nats.Connect(
		natsUrl,
		nats.ReconnectWait(30 * time.Second),
		nats.RetryOnFailedConnect(true),
		nats.Timeout(1 * time.Minute),
	)

	if err != nil {
		log.Fatal().
			Caller().
			Err(err).
			Msg("can't connect to nats")
	}

	stream, err = client.JetStream()

	if err != nil {
		log.Fatal().
			Caller().
			Err(err).
			Msg("can't create jetstream stream")
	}

	info, err := stream.StreamInfo("rtmp")

	if err == nats.ErrStreamNotFound {
		log.Fatal().
			Caller().
			Bool("is_closed", client.IsClosed()).
			Bool("is_connected", client.IsConnected()).
			Err(err).
			Msg("stream not found for `rtmp`")
	}

	log.Debug().
		Interface("stream", info).
		Msg("stream info")

	// don't need to check for consumerInfo since our consumer is stateless rather
	// than durable

	// consumerInfo, err := stream.AddConsumer(info.Config.Name, &nats.ConsumerConfig{
	// 	DeliverPolicy: nats.DeliverAllPolicy,
	// 	ReplayPolicy: nats.ReplayInstantPolicy,
	// 	AckPolicy: nats.AckExplicitPolicy,
	// 	AckWait: 30 * time.Minute,
	// 	Heartbeat: 1 * time.Minute,
	// })

	// if err != nil {
	// 	log.Fatal().Err(err).Msg("can't create consumer")
	// }

	// log.Debug().
	// 	Interface("consumer", consumerInfo).
	// 	Msg("consumer info")
}

func main() {

	subscription, err := stream.QueueSubscribeSync(
		"rtmp.events", "rtmp",
		nats.ManualAck(),
		nats.AckExplicit(),
		// nats.MaxAckPending(1),
		nats.AckWait(ackWaitDuration),
		// nats.IdleHeartbeat(idleDuration),
		nats.ReplayInstant(),
		nats.DeliverAll(),
	)

	if err != nil {

		log.Fatal().
			Caller().
			Err(err).
			Str("stream", "rtmp-event").
			Str("queue", "rtmp-event-queue").
			Msg("can't subscribe to `rtmp-event` with queue `rtmp-event-queue`")

	}

	for {
		message, err := subscription.NextMsg(30 * time.Second)

		if err == nats.ErrTimeout {
			log.Warn().
				Caller().
				Err(err).
				Str("queue", subscription.Queue).
				Str("subject", subscription.Subject).
				Msg("subscription timeout when fetching next message")


			if client.IsClosed() || !client.IsConnected() {
				log.Fatal().
					Caller().
					Err(err).
					Str("queue", subscription.Queue).
					Str("subject", subscription.Subject).
					Msg("client disconnected")
			}

			continue
		}

		log.Debug().
			Err(err).
			Str("queue", subscription.Queue).
			Str("subject", subscription.Subject).
			Str("message", string(message.Data)).
			Msg("got message")

		if err = message.InProgress(nats.AckWait(ackWaitDuration)); err != nil {
			// keep the heartbeat for in progress ack
			log.Error().
				Caller().
				Err(err).
				Str("queue", subscription.Queue).
				Str("subject", subscription.Subject).
				Str("message", string(message.Data)).
				Msg("unknown message")

			continue
		}

		signal := make(chan struct{}, 1)

		go func() {
			ticker := time.NewTicker(keepAliveDuration)

			log.Debug().
				Str("queue", subscription.Queue).
				Str("subject", subscription.Subject).
				Interface("", message.Header).
				Msg("message keepalive started")

			defer func() {
				log.Debug().
					Str("queue", subscription.Queue).
					Str("subject", subscription.Subject).
					Interface("", message.Header).
					Msg("message keepalive done")
			}()

			for {
				select {
				case <- ticker.C:
					if err = message.InProgress(nats.AckWait(ackWaitDuration)); err != nil {
						// keep the heartbeat for in progress ack
						log.Error().
							Caller().
							Err(err).
							Str("queue", subscription.Queue).
							Str("subject", subscription.Subject).
							Str("message", string(message.Data)).
							Msg("unknown message")
						return
					} else {
						log.Debug().
							Caller().
							Err(err).
							Str("queue", subscription.Queue).
							Str("subject", subscription.Subject).
							Str("message", string(message.Data)).
							Msg("message heartbeat keepalive")

					}
				case <-signal:
					return
				}
			}

		}()

		notifyEvent := event.NotifyEvent{}

		err = json.Unmarshal(message.Data, &notifyEvent)

		if err != nil {

			log.Error().
				Caller().
				Err(err).
				Str("queue", subscription.Queue).
				Str("subject", subscription.Subject).
				Str("message", string(message.Data)).
				Msg("unknown message")

			if err = message.Nak(); err != nil {
				log.Error().
					Caller().
					Err(err).
					Str("queue", subscription.Queue).
					Str("subject", subscription.Subject).
					Str("message", string(message.Data)).
					Msg("fails at nak the message")
			}

			signal <- struct{}{}

			continue
		}

		log.Info().
			Str("queue", subscription.Queue).
			Str("subject", subscription.Subject).
			Stringer("stream", notifyEvent.Stream).
			Stringer("event_type", notifyEvent.Type).
			Msgf("receiving event")

		taskDuration := workDuration()

		fmt.Println("task duration: ", taskDuration)

		time.Sleep(taskDuration)

		if err = message.AckSync(); err != nil {
			log.Error().
				Caller().
				Err(err).
				Str("queue", subscription.Queue).
				Str("subject", subscription.Subject).
				Stringer("stream", notifyEvent.Stream).
				Stringer("event_type", notifyEvent.Type).
				Msg("fails at ack the message")
		}

		signal <- struct{}{}
	}
}
