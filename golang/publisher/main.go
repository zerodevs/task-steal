package main

import (
	"encoding/json"
	"io/ioutil"
	"net/http"
	"os"
	"strings"
	"time"

	"github.com/nats-io/nats.go"
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	"gitlab.com/zerodevs/task-steal/event"
)

var (
	client * nats.Conn
	stream nats.JetStreamContext

	publishDuration time.Duration = 1 * time.Minute
	publishTimes int = 10
	natsUrl string = "nats://127.0.0.1:4222"
)

func ParseDefaultsFromEnvs() {
	var err error

	publishDurationRaw := os.Getenv("PUBLISH_DURATION")

	publishDurationRaw = strings.TrimPrefix(publishDurationRaw, " ")

	if publishDurationRaw != "" {
		publishDuration, err = time.ParseDuration(publishDurationRaw)

		if err != nil {
			log.Error().
				Caller().
				Err(err).
				Str("raw", publishDurationRaw).
				Msg("can't parse PUBLISH_DURATION")
		}
	}

	natsUrl := os.Getenv("NATS_URL")
	natsUrl = strings.TrimPrefix(natsUrl, " ")

	if natsUrl == "" {
		natsUrl = "nats://127.0.0.1:4222"
	}

	log.Debug().
		Str("url", natsUrl).
		Msg("debug envs")

	// publishTimesRaw := os.Getenv("")

}

func init() {
	log.Logger = log.Output(zerolog.ConsoleWriter{Out: os.Stderr})

	zerolog.SetGlobalLevel(zerolog.TraceLevel)

	var err error

	ParseDefaultsFromEnvs()

	client, err = nats.Connect(
		natsUrl,
		nats.DontRandomize(),
		nats.PingInterval(10 * time.Second),
		nats.ReconnectJitter(10 * time.Second, 15 * time.Second),
		nats.ReconnectWait(1 * time.Minute),
		nats.RetryOnFailedConnect(true),
		nats.Timeout(5 * time.Minute),
	)

	if err != nil {
		log.Fatal().
			Caller().
			Err(err).
			Msg("can't connect to nats")
	}

	if !client.IsConnected() || client.IsClosed() {

		hclient := http.DefaultClient

		resp, herr := hclient.Get("http://nats:8222/")

		if herr != nil {
			log.Fatal().
				Caller().
				Err(herr).
				Msg("can't contact to its http")
		}

		// defer resp.Body.Close()

		data, herr := ioutil.ReadAll(resp.Body)

		if herr != nil {
			log.Fatal().
				Caller().
				Err(herr).
				Msg("can't read the messages")
		}

		log.Debug().
			Caller().
			Str("resp", string(data)).
			Err(err).
			Msg("nats still not being connected :')")
	}

	stream, err = client.JetStream()

	if err != nil {
		log.Fatal().
			Err(err).
			Caller().
			Msg("can't create jetstream stream")
	}

	// check whether it's already being created before
	info, err := stream.StreamInfo("rtmp")

	// if it's not being created before
	if err == nats.ErrStreamNotFound {

		info, err = stream.AddStream(&nats.StreamConfig{
			Name: "rtmp",
			Storage: nats.FileStorage,
			NoAck: false,
			Subjects: []string{"rtmp.events"},
			// if worker ack the message, the message can be removed
			// https://github.com/nats-io/nats.go/blob/2ea8d393bbd4eb781462695533a0f1bf321ca7e5/js.go#L2689
			Retention: nats.WorkQueuePolicy,
			// fails to publish if full
			// https://github.com/nats-io/nats.go/blob/2ea8d393bbd4eb781462695533a0f1bf321ca7e5/js.go#L2701
			Discard: nats.DiscardNew,
		}, nats.PublishAsyncMaxPending(100))

		if err != nil {
			log.Fatal().
				Caller().
				Err(err).Msg("can't create stream for `rtmp`")
		}
	}

	if err != nil {
		log.Fatal().
			Caller().
			Bool("is_closed", client.IsClosed()).
			Bool("is_connected", client.IsConnected()).
			Err(err).
			Msg("unknown error")
	}

	log.Debug().
		Interface("info", info).
		Msg("stream info")

}

func main() {

	ticker := time.NewTicker(publishDuration)

	for {
		select {
		case <-ticker.C:

			publishEvent, err := event.RandomizedPublishEvent()

			if err != nil {
				log.Error().
					Caller().
					Err(err).Msgf("can't create publish events")
				continue
			}

			rawPublishEvent, err := json.Marshal(publishEvent)

			if err != nil {
				log.Error().
					Caller().
					Err(err).
					Stringer("stream_id", publishEvent.Stream).
					Interface("event", publishEvent).
					Msg("can't marshal publish events")

				continue
			}

			ack, err := stream.Publish(
				"rtmp.events",
				rawPublishEvent,
			)

			if err != nil {
				log.Error().
					Caller().
					Err(err).
					Stringer("stream_id", publishEvent.Stream).
					Interface("event", publishEvent).
					Msgf("can't send event %v", publishEvent.Stream)

				continue
			}

			log.Trace().
				Stringer("stream_id", publishEvent.Stream).
				Interface("ack", ack).
				Msg("event published")

		}
	}

}
