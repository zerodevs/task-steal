FROM golang:latest
RUN mkdir -p /app/task-steal
WORKDIR /app/task-steal
RUN mkdir -p /app/task-steal/event
COPY event /app/task-steal/event

subscriber:
	RUN mkdir -p /app/task-steal/subscriber
	COPY subscriber /app/task-steal/subscriber
	RUN mkdir -p /app/task-steal/subscriber/build
	WORKDIR /app/task-steal/subscriber
	RUN CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -v -a -mod=readonly -o subscriber
	SAVE ARTIFACT /app/task-steal/subscriber/subscriber /subscriber AS LOCAL build/subscriber

publisher:
	RUN mkdir -p /app/task-steal/publisher
	COPY publisher /app/task-steal/publisher
	RUN mkdir -p /app/task-steal/publisher/build
	WORKDIR /app/task-steal/publisher
	RUN CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -v -a -mod=readonly -o publisher
	SAVE ARTIFACT /app/task-steal/publisher/publisher /publisher AS LOCAL build/publisher

subscriber-image:
	FROM alpine:latest
	ARG VERSION
	COPY +subscriber/subscriber /usr/bin/
	ENTRYPOINT ["/usr/bin/subscriber"]
	SAVE IMAGE task-steal/golang/subscriber:$VERSION
	SAVE IMAGE task-steal/golang/subscriber:local

publisher-image:
	FROM alpine:latest
	ARG VERSION
	COPY +publisher/publisher /usr/bin/
	ENTRYPOINT ["/usr/bin/publisher"]
	SAVE IMAGE task-steal/golang/publisher:$VERSION
	SAVE IMAGE task-steal/golang/publisher:local

docker:
	ARG VERSION
	BUILD --build-arg VERSION=$VERSION +subscriber-image
	BUILD --build-arg VERSION=$VERSION +publisher-image

build:
	BUILD +subscriber
	BUILD +publisher

local:
	BUILD +build

all:
	ARG VERSION
	BUILD --build-arg VERSION=$VERSION +docker
