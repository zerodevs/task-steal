* task-stealer

** Overview

This give an example for doing task queue (with work-stealing enabled) in consumer using NATS JetStream.
This example give an example to do it in ~Golang~ (& soon ~Rust~).


** Development

*** General

- ~docker-compose~
  [[https://github.com/docker/compose/][docker compose]]

- ~docker-buildx~
  [[https://docs.docker.com/buildx/working-with-buildx/][docker buildx & buildkit]]

- ~earthly~
  [[https://github.com/earthly/earthly][earthly Container LLB Frontends]]

*** Golang
