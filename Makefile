.PHONY: deps compile docker

IMAGE_VERSION=$(shell git rev-parse --verify --short HEAD | tr -d \n)

deps:
	cd golang/publisher; go mod tidy
	cd golang/subscriber; go mod tidy

compile:
	cd golang/publisher; go build -v
	cd golang/subscriber; go build -v

docker:
	cd golang; earth --build-arg VERSION="${IMAGE_VERSION}" +docker
